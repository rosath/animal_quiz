package com.myapp.www.animalquiz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by Rosa on 11/04/2017.
 */

public class FinalQuiz extends Activity {

    private String name = "";
    private int totalPoints = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = getIntent().getStringExtra("userName");
        totalPoints = getIntent().getIntExtra("points", 0);
        setContentView(R.layout.final_quiz);
        //Set the title font
        setTitleFont();
        TextView txtFinalMessage = (TextView) findViewById(R.id.txt_congratulations);
        setFinalMessage(txtFinalMessage);
        TextView txtPointsMessage = (TextView) findViewById(R.id.txt_total_points);
        setFinalPointsMessage(txtPointsMessage);
    }

    /**
     * Set the title font
     */
    private void setTitleFont() {
        String routeFont = "fonts/junglefever_feverinlinenf.ttf";
        Typeface font = Typeface.createFromAsset(getAssets(), routeFont);
        TextView txtTitle = (TextView) findViewById(R.id.txt_title);
        txtTitle.setTypeface(font);
    }

    /**
     * When the back bottom button is pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(FinalQuiz.this, QuestionTen.class);
        i.putExtra("userName", name);
        totalPoints--;
        i.putExtra("points", totalPoints);
        startActivity(i);
    }

    /**
     * Set a TextView to display a message to the user according to the points obtained
     * @param txtFinalMessage  this TextView is for set the text to show
     */
    private void setFinalMessage(TextView txtFinalMessage) {
        if (totalPoints < 5) {
            txtFinalMessage.setText("Cheer up, " + name + "! \nI'm sure you make it better next time" );
        } else if (totalPoints >= 5 && totalPoints < 10) {
            txtFinalMessage.setText("Well done, " + name + "! \nYou know a lot about animals");
        } else {
            txtFinalMessage.setText("Congratulations, " + name + "! \nYou know a lot about animals");
        }
    }

    /**
     * Set a TextView for show the total points of the quiz
     * @param txtPointsMessage  this TextView is for set the text to show
     */
    private void setFinalPointsMessage(TextView txtPointsMessage) {
        txtPointsMessage.setText("Your final score is \n" + totalPoints);
    }
}
