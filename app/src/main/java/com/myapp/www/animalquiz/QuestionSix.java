package com.myapp.www.animalquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Rosa on 10/04/2017.
 */

public class QuestionSix extends Activity {

    private String name = "";
    private int totalPoints = 0;
    private RadioButton rbTwo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = getIntent().getStringExtra("userName");
        totalPoints = getIntent().getIntExtra("points", 0);
        setContentView(R.layout.question_six);
        TextView txtName = (TextView) findViewById(R.id.txt_name);
        txtName.setText(String.valueOf(name));
        rbTwo = (RadioButton) findViewById(R.id.rb_two_six);
        Button btnNext = (Button) findViewById(R.id.btn_next_six);
        // When the next button is clicked
        btnNext.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (optionSelected()) {
                    Intent i = new Intent(QuestionSix.this, QuestionSeven.class);
                    i.putExtra("userName", name);
                    calculatePoints();
                    i.putExtra("points", totalPoints);
                    startActivity(i);
                } else {
                    showMessage();
                }
            }
        });
    }

    /**
     * When the back bottom button is pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(QuestionSix.this, QuestionFive.class);
        i.putExtra("userName", name);
        totalPoints--;
        i.putExtra("points", totalPoints);
        startActivity(i);
    }

    /**
     * Calculate the total points for the question.
     */
    private void calculatePoints() {
        if (rbTwo.isChecked())
            totalPoints++;
    }

    /**
     * To check if the user has selected an option
     * @return true if the user has selected an option. False otherwise
     */
    private boolean optionSelected() {
        RadioButton rbOne = (RadioButton) findViewById(R.id.rb_one_six);
        RadioButton rbThree = (RadioButton) findViewById(R.id.rb_three_six);
        if (rbOne.isChecked() || rbTwo.isChecked() || rbThree.isChecked())
            return true;
        return false;
    }

    /**
     * Show a toast message to the user, if the next button is pressed before that the user introduce
     * the name
     */
    private void showMessage() {
        Toast.makeText(QuestionSix.this, "Please, select an option to continue", Toast.LENGTH_SHORT).show();
    }

}
