package com.myapp.www.animalquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by Rosa on 10/04/2017.
 */

public class QuestionOne extends Activity {

    private String name = "";
    private int totalPoints = 0;
    private RadioButton rbOne;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = getIntent().getStringExtra("userName");
        setContentView(R.layout.question_one);
        TextView txtName = (TextView) findViewById(R.id.txt_name);
        txtName.setText(String.valueOf(name));
        rbOne = (RadioButton) findViewById(R.id.rb_one_one);
        Button btnNext = (Button) findViewById(R.id.btn_next_one);
        // When the next button is clicked
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (optionSelected()) {
                    Intent i = new Intent(QuestionOne.this, QuestionTwo.class);
                    i.putExtra("userName", name);
                    calculatePoints();
                    i.putExtra("points", totalPoints);
                    startActivity(i);
                } else {
                    showMessage();
                }
            }
        });
    }

    /**
     * When the back bottom button is pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(QuestionOne.this, MainActivity.class);
        i.putExtra("userName", name);
        totalPoints = 0;
        i.putExtra("points", totalPoints);
        startActivity(i);
    }

    /**
     * Calculate the total points for the question.
     */
    private void calculatePoints() {
        if (rbOne.isChecked())
            totalPoints++;
    }

    /**
     * To check if the user has selected an option
     * @return  true if the user has selected an option. False otherwise
     */
    private boolean optionSelected() {
        RadioButton rbTwo = (RadioButton) findViewById(R.id.rb_two_one);
        RadioButton rbThree = (RadioButton) findViewById(R.id.rb_three_one);
        if (rbOne.isChecked() || rbTwo.isChecked() || rbThree.isChecked())
            return true;
        return false;
    }

    /**
     * Show a toast message to the user, if the next button is pressed before that the user introduce
     * the name
     */
    private void showMessage() {
        Toast.makeText(QuestionOne.this, "Please, select an option to continue", Toast.LENGTH_SHORT).show();
    }
}