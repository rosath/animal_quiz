package com.myapp.www.animalquiz;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        //Set the font
        setTitleFont();
        Button btnReady = (Button) findViewById(R.id.btn_ready);
        // When the next button is clicked
        btnReady.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = getName();
                if (isAName(name)) {
                    Intent i = new Intent(MainActivity.this, QuestionOne.class);
                    i.putExtra("userName", name);
                    startActivity(i);
                } else {
                    showMessage();
                }
            }
        });
    }

    /**
     * Set the title font
     */
    private void setTitleFont() {
        String routeFont = "fonts/junglefever_feverinlinenf.ttf";
        Typeface font = Typeface.createFromAsset(getAssets(), routeFont);
        TextView txtTitle = (TextView) findViewById(R.id.txt_title);
        txtTitle.setTypeface(font);
    }

    /**
     * Check if a name has been entered
     * @param candidateName the user name
     * @return              true if the user has entered a name. False otherwise
     */
    private boolean isAName(String candidateName) {
        if (candidateName.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Get the name introduced by the user
     * @return      the name of the user
     */
    private String getName() {
        EditText edtName = (EditText) findViewById(R.id.edt_name);
        return edtName.getText().toString();
    }

    /**
     * Show a toast message to the user, if the next button is pressed before that the user introduce
     * the name
     */
    private void showMessage() {
        Toast.makeText(MainActivity.this, "Please, enter a name to continue", Toast.LENGTH_SHORT).show();
    }
}
