package com.myapp.www.animalquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Rosa on 10/04/2017.
 */

public class QuestionFive extends Activity{

    private String name = "";
    private int totalPoints = 0;
    private CheckBox chbTwo;
    private CheckBox chbThree;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = getIntent().getStringExtra("userName");
        totalPoints = getIntent().getIntExtra("points", 0);
        setContentView(R.layout.question_five);
        TextView txtName = (TextView) findViewById(R.id.txt_name);
        txtName.setText(String.valueOf(name));
        chbTwo = (CheckBox) findViewById(R.id.chb_two_five);
        chbThree = (CheckBox) findViewById(R.id.chb_three_five);
        Button btnNext = (Button) findViewById(R.id.btn_next_five);
        // When the next button is clicked
        btnNext.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (optionSelected()) {
                    Intent i = new Intent(QuestionFive.this, QuestionSix.class);
                    i.putExtra("userName", name);
                    calculatePoints();
                    i.putExtra("points", totalPoints);
                    startActivity(i);
                } else {
                    showMessage();
                }
            }
        });
    }

    /**
     * When the back bottom button is pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(QuestionFive.this, QuestionFour.class);
        i.putExtra("userName", name);
        totalPoints--;
        i.putExtra("points", totalPoints);
        startActivity(i);
    }

    /**
     * Calculate the total points for the question.
     */
    private void calculatePoints() {
        if (chbTwo.isChecked() && chbThree.isChecked())
            totalPoints++;
    }

    /**
     * To check if the user has selected an option
     * @return true if the user has selected an option. False otherwise
     */
    private boolean optionSelected() {
        CheckBox chbOne = (CheckBox) findViewById(R.id.chb_one_five);
        if (chbOne.isChecked() || chbTwo.isChecked() || chbThree.isChecked())
            return true;
        return false;
    }

    /**
     * Show a toast message to the user, if the next button is pressed before that the user introduce
     * the name
     */
    private void showMessage() {
        Toast.makeText(QuestionFive.this, "Please, select at least one option to continue", Toast.LENGTH_SHORT).show();
    }
}
