package com.myapp.www.animalquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Rosa on 10/04/2017.
 */

public class QuestionFour extends Activity {

    private String name = "";
    private int totalPoints = 0;
    private CheckBox chbOne;
    private CheckBox chbThree;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = getIntent().getStringExtra("userName");
        totalPoints = getIntent().getIntExtra("points", 0);
        setContentView(R.layout.question_four);
        TextView txtName = (TextView) findViewById(R.id.txt_name);
        txtName.setText(String.valueOf(name));
        chbOne = (CheckBox) findViewById(R.id.chb_one_four);
        chbThree = (CheckBox) findViewById(R.id.chb_three_four);
        Button btnNext = (Button) findViewById(R.id.btn_next_four);
        // When the next button is clicked
        btnNext.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (optionSelected()) {
                    Intent i = new Intent(QuestionFour.this, QuestionFive.class);
                    i.putExtra("userName", name);
                    calculatePoints();
                    i.putExtra("points", totalPoints);
                    startActivity(i);
                } else {
                    showMessage();
                }
            }
        });
    }

    /**
     * When the back bottom button is pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(QuestionFour.this, QuestionThree.class);
        i.putExtra("userName", name);
        totalPoints--;
        i.putExtra("points", totalPoints);
        startActivity(i);
    }

    /**
     * Calculate the total points for the question.
     */
    private void calculatePoints() {
        if (chbOne.isChecked() && chbThree.isChecked())
            totalPoints++;
    }

    /**
     * To check if the user has selected an option
     * @return  true if the user has selected an option. False otherwise
     */
    private boolean optionSelected() {
        CheckBox chbTwo = (CheckBox) findViewById(R.id.chb_two_four);
        if (chbOne.isChecked() || chbTwo.isChecked() || chbThree.isChecked())
            return true;
        return false;
    }

    /**
     * Show a toast message to the user, if the next button is pressed before that the user introduce
     * the name
     */
    private void showMessage() {
        Toast.makeText(QuestionFour.this, "Please, select at least one option to continue", Toast.LENGTH_SHORT).show();
    }
}
